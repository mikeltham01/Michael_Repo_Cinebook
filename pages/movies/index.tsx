import React from 'react';
import { WithDefaultLayout } from '../../components/DefautLayout';
import { Title } from '../../components/Title';
import { Page } from '../../types/Page';
import useSWRinfinite from 'swr/infinite';

interface Movies {
    movieId: number,
    title: string,
    synopsis: string,
    startDate: string,
    endDate: string
}

const getKey = (pageIndex: number, previousPageData: Movies[]) => {
    // Default pageIndex = 0.
    pageIndex = pageIndex + 1;
    
    // If last page, don't fetch any more data.
    if (previousPageData && previousPageData.length === 0) {
        return null;
    }
    //return `/api/demo/api/Movie/get-movie?pageIndex=${pageIndex}&itemsPerPage=5`
    return '/api/demo/api/Movie/get-movie';
}

const fetcher = async (url: string) => {
    return await fetch(url).then((res) => res.json());
}

const MoviePage: Page = () => {
    const { data, error, size, setSize } = useSWRinfinite(getKey, fetcher);

    if (error) {
        return <div>Failed to load</div>;
    }
    if (!data) {
        return <div>Loading...</div>;
    }
    
    return (
        <div>
            <Title name="Movies"></Title>
            <h1>Movie List</h1>
            <table>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Synopsis</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                    </tr>
                </thead>
                <tbody>
                    {data.map((movies: Movies[], index: number) => {
                        return movies.map((movie) => (
                            <tr key={`row_${index}_${movie.movieId}`}>
                                <td>{movie.title}</td>
                                <td>{movie.synopsis}</td>
                                <td>{movie.startDate}</td>
                                <td>{movie.endDate}</td>
                            </tr>
                        ));
                    })}
                </tbody>
            </table>

            <button onClick={() => setSize(size + 1)}>Load More</button>
        </div>
    );
}

MoviePage.layout = WithDefaultLayout;
export default MoviePage;
